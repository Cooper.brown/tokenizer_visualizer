from flask import Flask, render_template, request
from tokenizer import Tokenizers

app = Flask(__name__)

colors = ['red', 'green', 'blue', 'orange', 'yellow', 'indigo', 'violet']


## Showed to kaden, Gabrial, Mac

@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        text = request.form.get('textarea')
        if text:
            tokenizer_choice = request.form.get('dropdown')
            tokenizer = Tokenizers(text)
            if tokenizer_choice == 'bert':
                tokenized_data = tokenizer.bert_word_piece()
                colorizer = list_colorizer_bert(tokenized_data, colors)
            elif tokenizer_choice == 'byte_pair':
                tokenized_data = tokenizer.byte_pair_dropout()
                colorizer = list_colorizer_byte_pair_dropout(tokenized_data, colors)
            elif tokenizer_choice == "byte_pair_dropout":
                tokenized_data = tokenizer.byte_pair_dropout()
                colorizer = list_colorizer_byte_pair_dropout(tokenized_data, colors)
            return render_template('index.html',
                                   tokenized_text=colorizer)
        else:
            render_template('index.html')
    return render_template('index.html')


def list_colorizer_bert(tokenized_data, colors):
    idx = 0
    out_list = []
    for token in tokenized_data:
        spacing = ' '
        if idx >= len(colors):
            idx = 0
        if token[0] == '#':
            token = token[2:]
            spacing = ''
        out_list.append((spacing + token, f"mark-{colors[idx]}"))
        idx += 1
    return out_list


def list_colorizer_byte_pair_dropout(tokenized_data, colors):
    idx = 0
    out_list = []
    tokenized_list = tokenized_data.split()
    for token in tokenized_list:
        if idx >= len(colors):
            idx = 0
        if token[0] == "@":
            out_list.append(('' + token[1:], f"mark-{colors[idx]}"))
            idx += 1
        else:
            out_list.append((' ' + token, f"mark-{colors[idx]}"))
            idx += 1
    return out_list


if __name__ == '__main__':
    app.run(host='0.0.0.0')
