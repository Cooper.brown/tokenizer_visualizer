import re
from typing import List, Any

import word_piece_tokenizer
from word_piece_tokenizer import WordPieceTokenizer
from custom_tokenizers.bytepair import BytePair, BytePairDropout


class Tokenizers:
    def __init__(self, text):
        self.text = text

    def bert_word_piece(self) -> list:
        # https://pypi.org/project/word-piece-tokenizer/
        tokenizer = WordPieceTokenizer()
        ids = tokenizer.tokenize(self.text)
        return tokenizer.convert_ids_to_tokens(ids)[1:-1]

    def byte_pair_encoding(self) -> list[str | Any]:
        # https://towardsdatascience.com/byte-pair-encoding-subword-based-tokenization-algorithm-77828a70bee0
        # https://www.geeksforgeeks.org/byte-pair-encoding-bpe-in-nlp/
        bp = BytePair(self.text)
        encoded_text = bp.byte_pair_encoding()
        list_out = []
        for word in self.text.split():
            word = re.sub(r'\W+', '', word)
            for token in encoded_text.items():
                a = ''.join(token[0][:-4].split())
                print(f'token {a} word {word}')

                if token[0][:-4] == word:
                    print(f'token {token[:-4]} word {word}')
                    list_out.append(word)
                    continue
                if ''.join(token[0][:-4].split()) == word:
                    list_out.append(token[0][:-4])
                    continue
        return list_out

    def unigram_tokenization(self):
        # https://huggingface.co/docs/transformers/tokenizer_summary#unigram
        # https://huggingface.co/learn/nlp-course/chapter6/7?fw=pt
        pass

    def byte_pair_dropout(self):
        # https://github.com/VProv/BPE-Dropout
        # https: // github.com / VKCOM / YouTokenToMe
        # https://aclanthology.org/2020.acl-main.170/
        bpd = BytePairDropout('static/data/subword_nmt.voc')
        return bpd.tokenize_text(self.text)

    def dynamic_programing_encoding(self):
        # https://aclanthology.org/2020.acl-main.275.pdf
        pass


# text = """The Norwegian Elkhound is a robust spitz type known for his lush silver-gray coat and dignified but
#  friendly demeanor. The durable Elkhound is among Europe's oldest dogs. They sailed with the Vikings and figure in
#  Norse art and legend. Norwegian Elkhounds are hardy, short-bodied dogs standing about 20 inches at the shoulder. They
#  have a dense silver-gray coat and a tail curling tightly over the back. The deep chest, sturdy legs, and muscular
#  thighs belong to a dog built for an honest day's work. The eyes are a dark brown and the ears mobile and erect.
#  Overall, an Elkhound is the picture of an alert and steadfast dog of the north. Elkhounds are famously fine
#  companions and intelligent watchdogs. Agility and herding trials are good outlets for their natural athleticism and
#  eagerness. Reserved until introductions are made, an Elkhound is a trustworthy friend ever after. These strong,
#  confident dogs are truly sensitive souls, with a dash of houndy independence."""
#
#
# tkn = Tokenizers(text)
# print(tkn.byte_pair_dropout())