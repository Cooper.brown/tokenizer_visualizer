function autofillOnClick(){
    let mainTextArea = document.getElementById("mainTextArea")
    let mainForm = document.getElementById("mainForm")
    mainTextArea.value = exampleText
    // mainForm.submit()
}
let exampleText = "The Norwegian Elkhound is a robust spitz type known for his lush silver-gray coat and dignified but \n" +
    "friendly demeanor. The durable Elkhound is among Europe's oldest dogs. They sailed with the Vikings and figure in \n" +
    "Norse art and legend. Norwegian Elkhounds are hardy, short-bodied dogs standing about 20 inches at the shoulder. They \n" +
    "have a dense silver-gray coat and a tail curling tightly over the back. The deep chest, sturdy legs, and muscular \n" +
    "thighs belong to a dog built for an honest day's work. The eyes are a dark brown and the ears mobile and erect. \n" +
    "Overall, an Elkhound is the picture of an alert and steadfast dog of the north. Elkhounds are famously fine \n" +
    "companions and intelligent watchdogs. Agility and herding trials are good outlets for their natural athleticism and \n" +
    "eagerness. Reserved until introductions are made, an Elkhound is a trustworthy friend ever after. These strong, \n" +
    "confident dogs are truly sensitive souls, with a dash of houndy independence."