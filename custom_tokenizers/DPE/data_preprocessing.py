import custom_tokenizers

# Define your training text data
training_text = [
    "This is an example sentence.",
    "Another example sentence here.",
    # Add more sentences as needed
]

# Initialize and train a BPE tokenizer
tokenizer = custom_tokenizers.ByteLevelBPETokenizer()
trainer = custom_tokenizers.Trainer(special_tokens=["[PAD]", "[CLS]", "[SEP]", "[MASK]", "[UNK]"])
tokenizer.train_from_iterator(training_text, trainer=trainer)

# Encode your training data using the trained BPE tokenizer
training_data = []

for sentence in training_text:
    encoding = tokenizer.encode(sentence)
    input_sequence = encoding.ids  # Subword indices
    target_sequence = encoding.ids  # Same as input for unsupervised BPE training
    training_data.append((input_sequence, target_sequence))
