import torch
import torch.nn as nn
import torch.optim as optim


# Define a simple feedforward neural network
class SubwordSegmentationModel(nn.Module):
    def __init__(self, input_size, hidden_size, output_size):
        super(SubwordSegmentationModel, self).__init__()
        self.fc1 = nn.Linear(input_size, hidden_size)
        self.relu = nn.ReLU()
        self.fc2 = nn.Linear(hidden_size, output_size)
        self.softmax = nn.Softmax(dim=1)

    def forward(self, x):
        x = self.fc1(x)
        x = self.relu(x)
        x = self.fc2(x)
        return self.softmax(x)


# Define model hyperparameters
input_size = 100  # Dimension of subword embeddings
hidden_size = 64
output_size = len(subword_vocab)  # Size of the subword vocabulary

# Instantiate the model and optimizer
model = SubwordSegmentationModel(input_size, hidden_size, output_size)
optimizer = optim.Adam(model.parameters(), lr=0.001)

# Define the loss function (cross-entropy loss)
criterion = nn.CrossEntropyLoss()
model

import custom_tokenizers

# Define your training text data
training_text = [
    "This is an example sentence.",
    "Another example sentence here.",
    # Add more sentences as needed
]

# Initialize and train a BPE tokenizer
tokenizer = custom_tokenizers.ByteLevelBPETokenizer()
trainer = custom_tokenizers.Trainer(special_tokens=["[PAD]", "[CLS]", "[SEP]", "[MASK]", "[UNK]"])
tokenizer.train_from_iterator(training_text, trainer=trainer)

# Encode your training data using the trained BPE tokenizer
training_data = []

for sentence in training_text:
    encoding = tokenizer.encode(sentence)
    input_sequence = encoding.ids  # Subword indices
    target_sequence = encoding.ids  # Same as input for unsupervised BPE training
    training_data.append((input_sequence, target_sequence))
