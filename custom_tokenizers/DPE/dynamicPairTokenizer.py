class DynamicProgramingEncoding:
    def __init__(self, subword_vocab):
        self.subword_vocab = subword_vocab
        self.language_model = None

    def tokenize_word(self, word):
        word_len = len(word)

        best_score = [float('-inf')] * (word_len + 1)
        best_bound = [-1] * (word_len + 1)

        for end in range(1, word_len + 1):
            for start in range(end):
                subword = word[start:end]
                if subword in self.subword_vocab:
                    score = best_score[start] + self.language_model[subword]
                    if score > best_score[end]:
                        best_score[end] = score
                        best_bound[end] = start
        segmentation = []
        end = word_len
        while end > 0:
            start = best_bound[end]
            segmentation.append(word[start:end])
            end = start

        segmentation.reverse()

        return segmentation
