import heapq
import re
from collections import defaultdict
from heapq import heapify, heappop, heappush

import numpy as np


class BytePair:
    def __init__(self, text):
        self.text = text.split('.')
        self.vocab = defaultdict(int)
        self.n = 230

    def get_stats(self):
        pairs = defaultdict(int)
        for word, freq in self.vocab.items():
            symbols = word.split()
            for i in range(len(symbols) - 1):
                pairs[symbols[i], symbols[i + 1]] += freq
        return pairs

    def merge_vocab(self, pair):
        vocab_out = {}
        bigram = re.escape(' '.join(pair))
        p = re.compile(r'(?<!\S)' + bigram + r'(?!\S)')
        for word in self.vocab:
            w_out = p.sub(''.join(pair), word)
            vocab_out[w_out] = self.vocab[word]
        self.vocab = vocab_out

    def get_vocab(self):
        for line in self.text:
            for word in line.split():
                self.vocab[' '.join(list(word)) + ' </w>'] += 1
        return self.vocab

    def byte_pair_encoding(self):
        self.get_vocab()
        for i in range(self.n):
            pairs = self.get_stats()
            best = max(pairs, key=pairs.get)
            self.merge_vocab(best)
        return self.vocab


class BytePairDropout:
    def __init__(self, path, random_seed = None, dropout = 0.0):
        pass
        self.sentinels = ['##', '$']
        self.random_generator = np.random.RandomState(random_seed)
        self.dropout = dropout

        # Setting merge table
        self.merge_table = dict()
        priority = 1
        with open(path) as file:
            for line in file:
                one, two = line.rstrip('\n').split(' ')
                self.merge_table[(one, two)] = priority
                priority += 1


    def tokenize_word(self, word):
        subword_tokens = list(word)

        subword_tokens = [self.sentinels[0] + subword_tokens[0]] + subword_tokens[1:]
        subword_tokens = subword_tokens[:-1] + [subword_tokens[-1] + self.sentinels[1]]

        heap = []

        for x in range(len(subword_tokens)-1):
            current_pair = (subword_tokens[x], subword_tokens[x + 1])
            if current_pair in self.merge_table:
                priority = self.merge_table[current_pair]
                heap.append([priority, x])
        heapify(heap)

        subword_len = len(subword_tokens)
        dropped_merges = []

        while len(heap):
            priority, position = heappop(heap)

            if position > subword_len -2:
                continue
            current = subword_tokens[position]
            nxt = subword_tokens[position + 1]

            if self.merge_table.get((current, nxt), None) != priority:
                continue

            if self.random_generator.rand() < self.dropout:
                dropped_merges.append([priority, position])
                continue

            subword_tokens[position:position+2] = [current + nxt]
            subword_len -= 1

            for pair in heap:
                if pair[1] > position:
                    pair[1] -= 1

            for dropped_priority, dropped_position in dropped_merges:
                if dropped_position > position:
                    position -= 1
                heappush(heap, [dropped_priority, dropped_position])

            dropped_merges = []

            new_current = subword_tokens[position]
            if position > 0:
                previous = subword_tokens[position -1]
                if (previous, new_current) in self.merge_table:
                    heappush(heap, [self.merge_table[(previous, new_current)], position - 1])

            if position < (subword_len - 1):
                new_next = subword_tokens[position + 1]
                if (new_current, new_next) in self.merge_table:
                    heappush(heap, [self.merge_table[(new_current, new_next)], position - 1])

        subword_tokens[0] = subword_tokens[0].replace(self.sentinels[0], '')
        subword_tokens[-1] = subword_tokens[-1].replace(self.sentinels[1], '')

        bpe_symbol = '@'
        for x in range(1, subword_len):
            subword_tokens[x] = bpe_symbol + subword_tokens[x]

        if subword_tokens[0] == '':
            subword_tokens = subword_tokens[1:]
            subword_tokens[0] = subword_tokens[0].lstrip(bpe_symbol)

        if subword_tokens[-1] == bpe_symbol:
            subword_tokens.pop()

        return subword_tokens

    def tokenize_text(self, line):
        return ' '.join([' '.join(self.tokenize_word(word)) for word in line.split(' ')])
#
# text = """The Norwegian Elkhound is a robust spitz type known for his lush silver-gray coat and dignified but
# friendly demeanor. The durable Elkhound is among Europe's oldest dogs. They sailed with the Vikings and figure in
# Norse art and legend. Norwegian Elkhounds are hardy, short-bodied dogs standing about 20 inches at the shoulder. They
# have a dense silver-gray coat and a tail curling tightly over the back. The deep chest, sturdy legs, and muscular
# thighs belong to a dog built for an honest day's work. The eyes are a dark brown and the ears mobile and erect.
# Overall, an Elkhound is the picture of an alert and steadfast dog of the north. Elkhounds are famously fine
# companions and intelligent watchdogs. Agility and herding trials are good outlets for their natural athleticism and
# eagerness. Reserved until introductions are made, an Elkhound is a trustworthy friend ever after. These strong,
# confident dogs are truly sensitive souls, with a dash of houndy independence."""
#
# bp = BytePair(text)
# # print(bp.byte_pair_encoding())